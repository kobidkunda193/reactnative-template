/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {ApplicationProvider, Layout} from '@ui-kitten/components';
import * as eva from '@eva-design/eva';
import {mapping} from '@eva-design/eva';
import {default as customMapping} from './src/Style/custom-mapping';
import 'react-native-gesture-handler';
import Route from './src/Route';
import stores from './src/Action/index';
import {Provider} from 'mobx-react';
import {StatusBar} from 'react-native';
import {COLOR1} from './src/Style/theme';

const App: () => React$Node = () => {
  return (
    <>
      <Provider {...stores}>
        <ApplicationProvider
          {...eva}
          mapping={mapping}
          customMapping={customMapping}
          theme={eva.light}>
          <Layout style={{flex: 1}}>
            <StatusBar backgroundColor={COLOR1} />
            <Route />
          </Layout>
        </ApplicationProvider>
      </Provider>
    </>
  );
};

export default App;
