import {observable, computed, action} from 'mobx';
import AsyncStorage from '@react-native-community/async-storage';

export default class Auth {
  @observable username = 'ggg';
  @observable auth = null;
  @observable token = false;
  @observable renew_token = false;
  @observable refno = '444333';
}
