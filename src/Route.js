import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import Loading from './Views/Component/Pages/Loading';
import {inject, observer} from 'mobx-react';
import AsyncStorage from '@react-native-community/async-storage';
import Login from './Views/Component/Pages/Auth/Login';
import Dashboard from './Views/Component/Pages/Logged/Dashboard';
import Register from './Views/Component/Pages/Auth/Register';
import {PROFILE} from './Static/URL';
import {API_TOKEN} from './Static/credintials';
import Menulist from './Views/Component/Pages/Logged/Components/Menulist';
import Profile from './Views/Component/Pages/Logged/User/Profile';
import PendingTxt from './Views/Component/Pages/Logged/Components/PendingTxt';
import ScanQR from './Views/Component/Pages/Auth/ScanQR';
import TreeView from './Views/Component/Pages/Logged/Components/TreeView';
import TaskDetails from './Views/Component/Pages/Logged/Components/TaskDetails';
import DailyTask from './Views/Component/Pages/Logged/Components/DailtTask';
import TreeViewT from './Views/Component/Pages/Logged/Components/TreeViewT';
import CompleteTxt from './Views/Component/Pages/Logged/Components/CompleteTxt';
const Stack = createStackNavigator();
@inject('Auth')
@observer
class LoadingScreen extends Component {
  async componentDidMount() {
    //console.log(this.props.Auth.username);

    const value = await AsyncStorage.getItem('@access_token');
    if (value !== null) {
      console.log(value)
      let CHECK = await this.CheckLoggedin(value);
      if (CHECK === true) {
        this.props.Auth.auth = true;
      } else {
        this.props.Auth.auth = false;
      }
    } else {
      this.props.Auth.auth = false;
      console.log(this.props.Auth.username);
    }
  }

  CheckLoggedin = async value => {
    let CHECK_USER_LOGIN = await fetch(PROFILE, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let CON_CHECK_USER_LOGIN = await CHECK_USER_LOGIN.json();
    return CHECK_USER_LOGIN.status === 200;
  };

  render() {
    if (this.props.Auth.auth === true) {
      return (
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}>
            <Stack.Screen name="Dashboard" component={Dashboard} />
            <Stack.Screen name="Menulist" component={Menulist} />
            <Stack.Screen name="Profile" component={Profile} />
            <Stack.Screen name="PendingTxt" component={PendingTxt} />
            <Stack.Screen name="ScanQR" component={ScanQR} />
            <Stack.Screen name="TreeView" component={TreeView} />
            <Stack.Screen name="TreeViewT" component={TreeViewT} />
            <Stack.Screen name="DailyTask" component={DailyTask} />
            <Stack.Screen name="TaskDetails" component={TaskDetails} />
            <Stack.Screen name="CompleteTxt" component={CompleteTxt} />
          </Stack.Navigator>
        </NavigationContainer>
      );
    } else if (this.props.Auth.auth === false) {
      return (
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}>
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="ScanQR" component={ScanQR} />
          </Stack.Navigator>
        </NavigationContainer>
      );
    } else {
      return (
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}>
            <Stack.Screen name="Loading" component={Loading} />
          </Stack.Navigator>
        </NavigationContainer>
      );
    }
  }
}

function Route() {
  return <LoadingScreen />;
}

export default Route;
