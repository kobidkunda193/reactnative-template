import React, {Component} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import {Container, SubContainer} from '../../../Style/theme';
import LottieView from 'lottie-react-native';
let HEIGHT = Dimensions.get('screen').height;
let WIDTH = Dimensions.get('screen').width;
export default class Loader extends Component {
  static navigationOptions: {
    headerShown: false,
  };

  componentDidMount() {
    this.animation.play();
    // Or set a specific startFrame and endFrame with:
    this.animation.play(0, 333);
  }

  render() {
    return (
      <Container>
        <SubContainer>
          <LottieView
            style={styles.lottiefile}
            ref={animation => {
              this.animation = animation;
            }}
            source={require('../../../Asset/lottie/loading')}
          />
        </SubContainer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  lottiefile: {
    width: WIDTH,
    height: HEIGHT,
  },
});
