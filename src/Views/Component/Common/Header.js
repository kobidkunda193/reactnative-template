import React, {Component} from 'react';
import {Dimensions, Text, View} from 'react-native';
import {TEXTLG} from '../../../Style/theme';
let HEIGHT = Dimensions.get('screen').height;
let WIDTH = Dimensions.get('screen').width;
export default class Header extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'ffffff',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 8,
          },
          shadowOpacity: 0.46,
          shadowRadius: 11.14,

          elevation: 17,
        }}>
        <View style={{width: WIDTH / 2}} />
        <TEXTLG>COMPANY</TEXTLG>
        <View style={{width: WIDTH / 2}} />
        <TEXTLG>ThePayout</TEXTLG>
      </View>
    );
  }
}
