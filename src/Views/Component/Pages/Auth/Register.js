import React, {Component} from 'react';
import {Input, Button, Radio, RadioGroup, Text} from '@ui-kitten/components';
import {Formik} from 'formik';
import {COLOR1, COLOR4, Container, SubContainer} from '../../../../Style/theme';
import * as yup from 'yup';
import {LOGIN, REGISTER} from '../../../../Static/URL';
import {PASSWORD_ID} from '../../../../Static/credintials';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {inject, observer} from 'mobx-react';

@inject('Auth')
@observer
class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      gender: '',
      name: '',
      email: '',
      password: '',
      phone: '',
      reference: '',
      epin: '',
      id: '',
    };
  }

  onCheckedChange = index => {
    console.log(index);
    this.setState({
      gender: index,
    });
  };

  OnRegister = async values => {
    let DATA = {
      name: values.name,
      epin: values.epin,
      email: values.email,
      password: values.password,
      phone: values.phone,
      reference: values.reference,
      gender: this.state.gender,
    };

    let GET_AUTH_TOKEN = await fetch(REGISTER, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(DATA),
    });
    let iiii = GET_AUTH_TOKEN.json();
    console.log(DATA);
    console.log(iiii);
    if (GET_AUTH_TOKEN.status === 200) {
      this.props.navigation.goBack();
    } else {
      alert('Some Error. Try Again Later');
    }
  };

  OnCamera = async () => {
    this.props.navigation.navigate('ScanQR');
  };

  renderIcon = style => <Icon name={'frame'} size={22} color={COLOR1} />;

  render() {
    return (
      <Formik
        enableReinitialize={true}
        initialValues={{
          name: '',
          email: '',
          password: '',
          reference: this.props.Auth.refno,
          phone: '',
          epin: '',
        }}
        onSubmit={values => this.OnRegister(values)}
        validationSchema={yup.object().shape({
          phone: yup
            .number()
            .required()
            .positive()
            .min(5000000000)
            .max(9999999999)
            .integer(),
          reference: yup.string().required(),
          email: yup
            .string()
            .email()
            .required(),
          name: yup
            .string()
            .min(6)
            .required(),
          epin: yup.string().required(),
          password: yup
            .string()
            .min(6)
            .required(),
        })}>
        {({
          values,
          handleChange,
          errors,
          setFieldTouched,
          touched,
          isValid,
          handleSubmit,
        }) => (
          <Container>
            <SubContainer
              style={{
                flex: 2,
                elevation: 100,
              }}>
              <Input
                value={values.name}
                enablesReturnKeyAutomatically={true}
                autoCompleteType={'name'}
                onChangeText={handleChange('name')}
                onBlur={() => setFieldTouched('name')}
                placeholder="Your Name"
                status={touched.name && errors.name ? 'danger' : 'primary'}
                caption={touched.name && errors.name ? errors.name : ''}
              />
              <Input
                value={values.phone}
                keyboardType={'numeric'}
                enablesReturnKeyAutomatically={true}
                autoCompleteType={'tel'}
                onChangeText={handleChange('phone')}
                onBlur={() => setFieldTouched('phone')}
                placeholder="Your Mobile Number"
                status={touched.phone && errors.phone ? 'danger' : 'primary'}
                caption={touched.phone && errors.phone ? errors.phone : ''}
              />
              <Input
                value={values.email}
                enablesReturnKeyAutomatically={true}
                autoCompleteType={'email'}
                onChangeText={handleChange('email')}
                onBlur={() => setFieldTouched('email')}
                placeholder="E-mail"
                status={touched.email && errors.email ? 'danger' : 'primary'}
                caption={touched.email && errors.email ? errors.email : ''}
              />
              <Input
                value={values.password}
                enablesReturnKeyAutomatically={true}
                autoCompleteType={'password'}
                secureTextEntry={true}
                onChangeText={handleChange('password')}
                onBlur={() => setFieldTouched('password')}
                placeholder="Password"
                status={
                  touched.password && errors.password ? 'danger' : 'primary'
                }
                caption={
                  touched.password && errors.password ? errors.password : ''
                }
              />
              <Input
                value={values.epin}
                enablesReturnKeyAutomatically={true}
                onChangeText={handleChange('epin')}
                onBlur={() => setFieldTouched('epin')}
                placeholder="EPIN"
                status={touched.epin && errors.epin ? 'danger' : 'primary'}
                caption={touched.epin && errors.epin ? errors.epin : ''}
              />
              <Input
                value={values.reference}
                enablesReturnKeyAutomatically={true}
                onChangeText={handleChange('reference')}
                onBlur={() => setFieldTouched('reference')}
                placeholder="Your Sponcer ID"
                onIconPress={this.OnCamera}
                icon={this.renderIcon}
                status={
                  touched.reference && errors.reference ? 'danger' : 'primary'
                }
                caption={
                  touched.reference && errors.reference ? errors.reference : ''
                }
              />
              <RadioGroup
                selectedIndex={this.state.gender}
                onChange={this.onCheckedChange}>
                <Radio status="control" text="Male" />
                <Radio status="control" text="Female" />
                <Radio status="control" text="Others" />
              </RadioGroup>

              <Text>{this.props.Auth.refno}</Text>
              <Button
                onPress={handleSubmit}
                style={{
                  width: 200,
                }}
                status="primary">
                Register
              </Button>
            </SubContainer>
          </Container>
        )}
      </Formik>
    );
  }
}

export default Register;
