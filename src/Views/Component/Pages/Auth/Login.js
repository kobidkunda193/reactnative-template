import React, {Component} from 'react';
import {
  Platform,
  KeyboardAvoidingView,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {Input, Button, ButtonGroup} from '@ui-kitten/components';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {COLOR1, Container, SubContainer} from '../../../../Style/theme';
import * as Animatable from 'react-native-animatable';
import {inject, observer} from 'mobx-react';
import {LOGIN} from '../../../../Static/URL';
import {API_TOKEN} from '../../../../Static/credintials';

@inject('Auth')
@observer
class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      username: '',
      password: '',
      loading: false,
    };
  }

  componentDidMount() {}

  onPress = async () => {
    this.setState({
      loading: true,
    });

    let GET_AUTH_TOKEN = await fetch(LOGIN, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
        grant_type: 'password',
        client_id: '2',
        client_secret: API_TOKEN,
        provider: 'users',
      }),
    });
    console.log(GET_AUTH_TOKEN);

    if (GET_AUTH_TOKEN.status === 200) {
      let RECIEVE_AUTH_TOKEN = await GET_AUTH_TOKEN.json();
      console.log(RECIEVE_AUTH_TOKEN);
      this.setState({user: RECIEVE_AUTH_TOKEN.data, loading: false});

      await AsyncStorage.setItem('@token_type', RECIEVE_AUTH_TOKEN.token_type);
      await AsyncStorage.setItem(
        '@access_token',
        RECIEVE_AUTH_TOKEN.access_token,
      );
      await AsyncStorage.setItem(
        '@refresh_token',
        RECIEVE_AUTH_TOKEN.refresh_token,
      );
      this.setState({
        loading: false,
      });
      this.props.Auth.auth = true;
      console.log(this.props.Auth.auth);
    } else {
      this.setState({
        loading: false,
      });
      alert('Invalid Username / Password');
    }
  };
  renderIcon = style => <Icon name={'user'}  />;
  renderIconPassw = style => <Icon name={'options'} size={22} color={COLOR1} />;

  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.Os === 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <Container>
          <SubContainer
            style={{
              flex: 2,
              elevation: 100,
            }}>
            <Animatable.Image
              animation="pulse"
              easing="ease-in-out-cubic"
              iterationCount="infinite"
              style={{
                width: 250,
                height: 70,
              }}
              source={require('../../../../Asset/Image/logowhite.png')}
            />
          </SubContainer>

          <SubContainer
            style={{
              flex: 0.6,
            }}>
            <Input
              placeholder="Your Email "
              autoCompleteType={'email'}
              icon={this.renderIcon}
              autoFocus={true}
              onChangeText={text => this.setState({username: text})}
              textContentType={'emailAddress'}

              // value={value}
              // onChangeText={setValue}
            />
          </SubContainer>

          <SubContainer
            style={{
              flex: 0.6,
            }}>
            <Input
              placeholder="Your Password "
              autoCompleteType={'password'}
              onChangeText={text => this.setState({password: text})}
              secure={true}
              icon={this.renderIconPassw}
              secureTextEntry={true}
              textContentType={'password'}

              // value={value}
              // onChangeText={setValue}
            />
          </SubContainer>

          <SubContainer
            style={{
              flex: 0.6,
            }}>
            <Button
              icon={
                this.state.loading
                  ? () => <ActivityIndicator color="#ffffff" />
                  : null
              }
              onPress={this.onPress}
              style={{
                width: 200,
              }}
              status="primary">
              Login
            </Button>
          </SubContainer>

          <SubContainer>
            <ButtonGroup appearance="outline" status="control">
              <Button
                onPress={() => this.props.navigation.navigate('Register')}>
                Register Account
              </Button>
              <Button>Forgot Password</Button>
            </ButtonGroup>
          </SubContainer>
        </Container>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({});

export default Login;
