import React, {Component} from 'react';

import {
  AppRegistry,
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Dimensions,
} from 'react-native';
import {Button, Layout, Modal} from '@ui-kitten/components';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera as Camera} from 'react-native-camera';
import {inject, observer} from 'mobx-react';
import {COLOR1, COLOR4} from '../../../../Style/theme';
let HEIGHT = Dimensions.get('screen').height;
let WIDTH = Dimensions.get('screen').width;
@inject('Auth')
@observer
export default class ScanQR extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  onSuccess = async e => {
    this.props.Auth.refno = e.data;
    this.props.navigation.goBack();
  };

  render() {
    return (

        <QRCodeScanner
            containerStyle={styles.zeroContainer}
            cameraStyle={styles.cameraContainer}
          onRead={this.onSuccess}
          FlashMode={false}
            bottomContent={
           <View>
             <View style={{
               top: 20,
               width: 200,
               height: 200,
               borderWidth: 2,
               borderColor: 'rgba(255,255,255,0.68)',
             }}>
             </View>

           </View>
         }
        />

    );
  }
}

const styles = StyleSheet.create({
  comtainer:{
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:COLOR1,
    width:WIDTH,
    height:HEIGHT
  },
  buttonText: {
    fontSize: 18,
    color: COLOR1,
  },
  buttonTouchable: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    backgroundColor:'rgba(255,255,255,0.68)',
    width:200,
    height:50,
    borderRadius:22,
    elevation:20
  },
  buttonText1: {
    fontSize: 18,
    color: COLOR1,
  },
  buttonTouchable1: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    backgroundColor:'rgba(255,255,255,0.68)',
    width:200,
    height:50,
    borderRadius:22,
    elevation:20
  },

  zeroContainer: {
    height: 0,
    flex: 0,
  },

  cameraContainer: {
    height: Dimensions.get('window').height,
  },
});
