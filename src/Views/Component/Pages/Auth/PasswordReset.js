import React, {Component} from 'react';
import {Text, View, Dimensions, StyleSheet} from 'react-native';

let HEIGHT = Dimensions.get('screen').height;
let WIDTH = Dimensions.get('screen').width;

export default class PasswordReset extends Component {
  static navigationOptions = {
    title: 'PasswordReset',
    headerStyle: {
      backgroundColor: '#2eb79e',
    },
    headerTintColor: '#ffffff',
    headerTitleStyle: {
      fontWeight: 'bold',
      color: '#ffffff',
    },
  };

  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  render() {
    return (
      <View>
        <Text />
      </View>
    );
  }
}

const styles = StyleSheet.create({});
