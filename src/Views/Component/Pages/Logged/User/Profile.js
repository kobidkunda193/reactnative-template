import React, {Component} from 'react';
import {
  Text,
  Platform,
  KeyboardAvoidingView,
  StyleSheet,
  View,
  StatusBar,
  ScrollView,
} from 'react-native';
import {ListItem} from 'react-native-elements';
import QRCode from 'react-native-qrcode-svg';
import Share from 'react-native-share';

import {
  COLOR1,
  COLOR4,
  COLORTEXTSM,
  ContainerN,
  SubContainer,
} from '../../../../../Style/theme';
import {PROFILE} from '../../../../../Static/URL';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {ModernHeader} from '@freakycoder/react-native-header-view';
import HeaderLogo from '../Components/HeaderLogo';
import ProfileItems from '../../../../../Style/ProfileItems';

class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: [],
      loading: false,
      qrcode: '',
    };
  }

  async componentDidMount() {
    const value = await AsyncStorage.getItem('@access_token');
    let CHECK_USER_LOGIN = await fetch(PROFILE, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let CON_CHECK_USER_LOGIN = await CHECK_USER_LOGIN.json();
    console.log(CON_CHECK_USER_LOGIN);
    this.setState({
      user: CON_CHECK_USER_LOGIN,
    });
  }

  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.Os === 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <ScrollView>
          <StatusBar backgroundColor={COLOR1} />
          <ContainerN
            style={{
              backgroundColor: COLOR1,
            }}>
            <ModernHeader
              text={null}
              rightIconType="SimpleLineIcons"
              backgroundColor="#fdfdfd"
              rightIconName="menu"
              leftIconComponent={<HeaderLogo />}
              rightIconColor={COLOR1}
              rightIconOnPress={() =>
                this.props.navigation.navigate('Menulist')
              }
            />
            <SubContainer
              style={{
                marginTop: 50,
                flex: 2,
                backgroundColor: '#ffffff',
                width: 130,
                height: 130,
                borderRadius: 20,
              }}>
              <View
                style={{
                  borderWidth: 2,
                  borderColor: COLOR1,
                  padding: 10,
                  borderRadius: 20,
                }}>
                <QRCode color={COLOR1} value={this.state.user.super_id} />
              </View>
            </SubContainer>
            <View
              style={{
                marginTop: 20,
              }}>
              <Icon.Button
                style={{
                  borderColor: '#ffffff',
                  borderWidth: 2,
                  borderRadius: 20,
                }}
                backgroundColor={COLOR1}
                color="#ffffff"
                name={'share-alt'}
                onPress={() =>
                  Share.open(
                    Platform.select({
                      default: {
                        subject: 'title',
                        message:
                          'Hi Download this Application and Earn unlimited income. Down from ' +
                          'Playstore https://play.google.com/store/apps/details?id=com.tecions.payout. Dont forget to use my Sponcer ID ' +
                          this.state.user.referrer_string,
                      },
                    }),
                  )
                }
                size={44}>
                <COLORTEXTSM
                  style={{
                    color: '#ffffff',
                    fontSize: 18,
                    fontWeight: '700',
                  }}>
                  Scan this Code
                </COLORTEXTSM>
              </Icon.Button>
            </View>

            <SubContainer
              style={{
                flex: 2,
                width: '100%',
              }}>
              <ProfileItems
                heading={'Sponcer ID'}
                text={this.state.user.referrer_string}
              />
              <ProfileItems heading={'Name'} text={this.state.user.name} />
              <ProfileItems heading={'Email'} text={this.state.user.email} />
              <ProfileItems heading={'phone'} text={this.state.user.phone} />
              <ProfileItems
                heading={'Refer ID'}
                text={this.state.user.super_id}
              />
            </SubContainer>
          </ContainerN>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({});

export default Profile;
