import React, {Component} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Image,
  ScrollView,
  Text, TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {COLOR1, COLOR3, COLOR4, COLOR5} from '../../../../Style/theme';
import Carousel from 'react-native-snap-carousel';
import {ModernHeader} from '@freakycoder/react-native-header-view';
import HeaderLogo from './Components/HeaderLogo';
import {
  BASE_URL,
  DATED_TXT,
  GETTASK,
  LOGIN,
  SLIDER,
  TREEVIEW,
  WALLET_VAL,
} from '../../../../Static/URL';
import {SvgUri} from 'react-native-svg';
import {FlatGrid} from 'react-native-super-grid';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import Loader from '../../Common/Loader';
import {API_TOKEN} from '../../../../Static/credintials';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: [],
      super:'',
      loading: null,
      sliders: [],
      wallet: [],
      task: [],
    };
  }

  async componentDidMount() {
    const value = await AsyncStorage.getItem('@access_token');

    let USER_LIST = await fetch(TREEVIEW, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let WALLET = await fetch(WALLET_VAL, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let TASK = await fetch(GETTASK, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let SLIDERVAR = await fetch(SLIDER, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let DAYLI_INCOME = await fetch(DATED_TXT, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
      body: JSON.stringify({
        start_date: '21-03-2020',
        end_date: '23-03-2020',
      }),
    });

    let USER_LIST_DATA = await USER_LIST.json();
    let SLIDER_DATA = await SLIDERVAR.json();
    let WALLET_DATA = await WALLET.json();
    let TASK_DATA = await TASK.json();
    let DAYLI_INCOME_DATA = await DAYLI_INCOME.json();

    console.log(USER_LIST_DATA);

    this.setState({
      super: USER_LIST_DATA.super,
      user: USER_LIST_DATA.all_downline,
    //  sliders: SLIDER_DATA.sliders,
      wallet: WALLET_DATA,
      task: TASK_DATA,
      loading: false,
    });
  }

  _renderItem = ({item, index}) => {
    //console.log(this.state.sliders);
    return (
      <View style={styles.slide}>
        <SvgUri width={80} height={40} uri={BASE_URL + item.user_pic} />
        <Text
          style={{
            textAlign: 'center',
            fontSize: 13,
            textTransform: 'uppercase',
          }}>
          {item.name}
        </Text>
      </View>
    );
  };

  _renderSlider = ({item, index}) => {
    // console.log(this.state.sliders);
    return (
      <View style={styles.slide4}>
        <Image
          source={{uri: BASE_URL + item.slide_url}}
          style={{width: 200, height: 180, borderRadius: 33}}
        />
      </View>
    );
  };

  render() {
    let load = this.state.loading;
    const items = [
      {
        name: 'WALLET BALANCE',
        code: COLOR1,
        icon: 'wallet',
        route: 'Profile',
        value: '₹ ' + this.state.wallet.wallet_balance,
      },
      {
        name: 'PENDING TASK',
        code: COLOR4,
        icon: 'equalizer',
        route: 'DailyTask',
        value: +this.state.task.no_of_tasks_pending,
      },
      {
        name: 'COMPLETED TASK',
        code: COLOR3,
        icon: 'check',
        route: 'DailyTask',
        value: this.state.task.no_of_tasks_completed,
      },
      {
        name: 'INCOME TODAY',
        code: COLOR5,
        icon: 'energy',
        route: 'CompleteTxt',
        value: '₹ 0',
      },
    ];
    if (load === false) {
      return (
        <SafeAreaView>
          <ScrollView>
            <ModernHeader
              text={null}
              rightIconType="SimpleLineIcons"
              backgroundColor="#fdfdfd"
              rightIconName="menu"
              leftIconComponent={<HeaderLogo />}
              rightIconColor={COLOR1}
              rightIconOnPress={() =>
                this.props.navigation.navigate('Menulist')
              }
            />

            <View
              style={{
                flex: 1,
                paddingTop: 12,
                paddingLeft: 22,
                paddingBottom: 33,
              }}>
              <Text
                style={{
                  textAlign: 'left',
                  fontSize: 22,
                  color: '#a6a6a6',
                }}>
                Hello
              </Text>
              <View
                style={{
                  height: 2,
                }}>
                <Text />
              </View>
              <Text
                style={{
                  textAlign: 'left',
                  fontSize: 33,
                  color: COLOR1,
                  fontWeight: '700',
                }}>
                {this.state.super.name}
              </Text>
            </View>

            <View
              style={{
                flex: 2,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Carousel
                layout={'default'}
                style={styles.imgpicconsli}
                autoplay={true}
                autoplayDelay={0}
                autoplayInterval={1000}
                loopClonesPerSide={3}
                loop={true}
                ref={c => {
                  this._carousel = c;
                }}
                data={this.state.sliders}
                renderItem={this._renderSlider}
                sliderWidth={400}
                itemWidth={200}
              />
            </View>



            <View
              style={{
                flex: 1,
              }}>
              <FlatGrid
                itemDimension={130}
                items={items}
                style={styles.gridView}
                // staticDimension={300}
                // fixed
                // spacing={20}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                      onPress={()=> this.props.navigation.navigate(item.route)}
                    style={[
                      styles.itemContainer,
                      {backgroundColor: item.code},
                    ]}>
                    <Icon
                      name={`${item.icon}`}
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        textAlign: 'center',
                      }}
                      color={'#ffffff'}
                      size={33}
                    />
                    <Text style={styles.itemCode}>{item.value}</Text>
                    <Text style={styles.itemName}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              />
            </View>
          </ScrollView>
        </SafeAreaView>
      );
    } else {
      return <Loader />;
    }
  }
}

const styles = StyleSheet.create({
  wrapper: {
    height: 200,
  },
  slide: {
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    borderRadius: 70,
    elevation: 20,
    borderColor: COLOR1,
    borderWidth: 2,
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR1,
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR1,
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR1,
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  scrollView: {
    marginHorizontal: 20,
  },
  imgpiccon: {
    height: 100,
  },
  imgpicconsli: {
    height: 300,
  },
  slide4: {
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 100,
  },
  gridView: {
    marginTop: 20,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 120,
    elevation: 20,
  },
  itemName: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: '600',
    fontSize: 33,
    color: '#fff',
  },
});

export default Dashboard;
