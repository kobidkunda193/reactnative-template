import React, {Component} from 'react';
import {Text, View, Dimensions, StyleSheet, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {
  ALLARTICLE,
  GETTASK,
  SLIDER,
  TXTPENDING,
} from '../../../../../Static/URL';
import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {
  COLOR0,
  COLOR1,
  COLOR4,
  COLOR5,
  COLORTEXTSM,
  SubContainer,
} from '../../../../../Style/theme';

let HEIGHT = Dimensions.get('screen').height;
let WIDTH = Dimensions.get('screen').width;
import {useNavigation} from '@react-navigation/native';
import HeaderLogo from './HeaderLogo';
import {ModernHeader} from '@freakycoder/react-native-header-view';

class DailyTask extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tasksp: [],
      tasksc: [],
      data: [],
    };
  }

  async componentDidMount() {
    const value = await AsyncStorage.getItem('@access_token');

    let GETSLIDER = await fetch(GETTASK, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let GETSLIDERPP = await GETSLIDER.json();
    console.log(GETSLIDERPP);
    this.setState({
      tasksp: GETSLIDERPP.tasks_pending,
      tasksc: GETSLIDERPP.tasks_completed,
      data: GETSLIDERPP.tasks,
    });
  }

  renderItem = ({item, navigation}) => (
    <ListItem
        style={{flex: 1}}
      title={'Task ' + item.task}
      titleStyle={{
        textTransform: 'uppercase',
      }}
      subtitle={item.is_complete === 0 ? 'pending' : 'complete'}
      leftIcon={
        <Icon //check
          name={item.is_complete === 0 ? 'speech' : 'check'}
          color={item.is_complete === 0 ? COLOR4 : COLOR5}
        />
      }
      onPress={() =>
        this.props.navigation.navigate('TaskDetails', {
          taskid: item.id,
          articleid: item.tasks_to_article.id,
        })
      }
      // onPress={() => this.props.navigation.navigate(`${item.url}`)}
      bottomDivider
      chevron
    />
  );

  render() {
    return (
      <View style={{  flex: 1, width: '100%' }}>
        <ModernHeader
          text={null}
          rightIconType="SimpleLineIcons"
          backgroundColor="#fdfdfd"
          rightIconName="menu"
          leftIconComponent={<HeaderLogo />}
          rightIconColor={COLOR1}
          rightIconOnPress={() => this.props.navigation.navigate('Menulist')}
        />
        <FlatList
            contentContainerStyle={{paddingBottom:60}}
            style={{
              paddingBottom:30
            }}
          keyExtractor={this.keyExtractor}
          data={this.state.data}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({});
export default DailyTask;
