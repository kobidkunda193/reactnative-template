import React, {Component} from 'react';
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {
  BASE_URL,
  SLIDER,
  TREEVIEW,
  TREEVIEWUUID,
  TXTPENDING,
} from '../../../../../Static/URL';
import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {
  COLOR1,
  COLOR2,
  COLOR4,
  COLOR5,
  COLORTEXTSM,
  SubContainer,
} from '../../../../../Style/theme';
import {Button} from '@ui-kitten/components';
import {SvgUri} from 'react-native-svg';
import HeaderLogo from './HeaderLogo';
import {ModernHeader} from '@freakycoder/react-native-header-view';
let HEIGHT = Dimensions.get('screen').height;
let WIDTH = Dimensions.get('screen').width;

export default class TreeView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      downline: [],
      load: false,
    };
  }

  async componentDidMount() {
    const value = await AsyncStorage.getItem('@access_token');

    let {route, navigation} = this.props;
    const {uuid} = route.params;

    let GETSLIDER = await fetch(TREEVIEWUUID + uuid, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let GETSLIDERPP = await GETSLIDER.json();
    console.log(GETSLIDERPP);
    console.log(GETSLIDERPP.level1.length);

    if (GETSLIDERPP.level1.length < 1) {
      console.log('1');
      this.setState({
        data: GETSLIDERPP.super,
        downline: GETSLIDERPP.level1,
        load: false,
      });
    } else if (GETSLIDERPP.level1.length > 0) {
      console.log('2');

      this.setState({
        data: GETSLIDERPP.super,
        downline: GETSLIDERPP.level1,
        load: true,
      });

      console.log(this.state.downline);
    } else {
      console.log('3');
      this.setState({
        data: GETSLIDERPP.super,
        load: false,
      });
    }
  }

  render() {
    let USER = this.state.data;
    let initialArr = this.state.downline;
    return (
      <ScrollView>
        <ModernHeader
          text={null}
          rightIconType="SimpleLineIcons"
          backgroundColor="#fdfdfd"
          rightIconName="menu"
          leftIconComponent={<HeaderLogo />}
          rightIconColor={COLOR1}
          rightIconOnPress={() => this.props.navigation.navigate('Menulist')}
        />
        <View style={styles.superContailer}>
          <View style={styles.slide1}>
            <TouchableOpacity onPress={this._handleAddButton}>
              <SvgUri
                width={80}
                height={40}
                uri={BASE_URL + '/default/user.svg'}
              />
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 18,
                  textTransform: 'uppercase',
                  color: '#ffffff',
                  fontWeight: '700',
                }}>
                {USER.name}
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 13,
                  textTransform: 'uppercase',
                  color: '#ffffff',
                  fontWeight: '700',
                }}>
                You
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.superContailer}>
          {this.state.load === true && (
            <View>
              {initialArr.map((item, key) => {
                return (
                  <View style={styles.slide}>
                    <TouchableOpacity
                      onPress={async () => {
                        const value = await AsyncStorage.getItem(
                          '@access_token',
                        );

                        let GETSLIDER2 = await fetch(TREEVIEWUUID + item.uuid, {
                          method: 'GET',
                          headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            Authorization: 'Bearer ' + value,
                          },
                        });

                        let GETSLIDERPP2 = await GETSLIDER2.json();
                        console.log(GETSLIDERPP2);
                        console.log(GETSLIDERPP2.level1.length);

                        if (GETSLIDERPP2.level1.length < 1) {
                          console.log('1');
                          this.setState({
                            data: GETSLIDERPP2.super,
                            downline: GETSLIDERPP2.level1,
                            load: false,
                          });
                        } else if (GETSLIDERPP2.level1.length > 0) {
                          console.log('2');

                          this.setState({
                            data: GETSLIDERPP2.super,
                            downline: GETSLIDERPP2.level1,
                            load: true,
                          });

                          console.log(this.state.downline);
                        } else {
                          console.log('3');
                          this.setState({
                            data: GETSLIDERPP2.super,
                            load: false,
                          });
                        }
                      }}
                      key={item.id}>
                      <SvgUri
                        width={80}
                        height={40}
                        uri={BASE_URL + '/default/user.svg'}
                      />
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 18,
                          textTransform: 'uppercase',
                          color: '#ffffff',
                          fontWeight: '700',
                        }}>
                        {item.name}
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              })}
            </View>
          )}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  superContailer: {
    alignContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    paddingTop: 20,
  },
  iconcom: {
    paddingTop: 5,
    alignContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
  },
  slide: {
    height: 120,
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR1,
    borderRadius: 22,
    elevation: 20,
    borderColor: '#9599ff',
    borderWidth: 2,
  },

  slide1: {
    height: 120,
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR5,
    borderRadius: 22,
    elevation: 7,
    borderColor: '#6edca8',
    borderWidth: 5,
  },
});
