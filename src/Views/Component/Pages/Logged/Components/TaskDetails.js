import React, {Component} from 'react';
import {Text, View, Dimensions, StyleSheet, ScrollView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigation} from '@react-navigation/native';
import {
  ALLARTICLE,
  ARTICLEDETAILS,
  COMPLETEGETTASK,
  GETTASK,
  LOGIN,
  SLIDER,
  TASKDETAILS,
  TXTPENDING,
} from '../../../../../Static/URL';
import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {
  COLOR1,
  COLOR4,
  COLORTEXTSM,
  SubContainer,
} from '../../../../../Style/theme';
import LottieView from 'lottie-react-native';
import {Button} from '@ui-kitten/components';
import {API_TOKEN} from '../../../../../Static/credintials';

import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';
import HeaderLogo from './HeaderLogo';
import {ModernHeader} from '@freakycoder/react-native-header-view';
import HTML from 'react-native-render-html';

let HEIGHT = Dimensions.get('screen').height;
let WIDTH = Dimensions.get('screen').width;

class TaskDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      task: [],
      article: [],
      timer: 10,
      taskid: '',
    };
  }

  async componentDidMount() {
    let {route, navigation} = this.props;
    const value = await AsyncStorage.getItem('@access_token');
    const {taskid} = route.params;
    const {articleid} = route.params;

    let GETSLIDER = await fetch(TASKDETAILS + taskid, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let GETSLIDERPP = await GETSLIDER.json();

    let GETARTICLE = await fetch(ARTICLEDETAILS + articleid, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let GETARTICLEPP = await GETARTICLE.json();
    console.log(GETARTICLEPP)
    this.setState({
      article: GETARTICLEPP.article,
      task: GETSLIDERPP,
      taskid: taskid,
    });
    this.animation.play();
    // Or set a specific startFrame and endFrame with:
    this.animation.play(1, 1000);
    this.startTimer();
  }

  startTimer = () => {
    this.clockCall = setInterval(() => {
      this.decrementClock();
    }, 1000);
  };

  decrementClock = () => {
    if (this.state.timer === 0) {
      clearInterval(this.clockCall);
    }
    this.setState(prevstate => ({timer: prevstate.timer - 1}));
  };

  onTsdkcom = async () => {
    const value = await AsyncStorage.getItem('@access_token');

    let GETSLIDER = await fetch(COMPLETEGETTASK + this.state.taskid, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let GETSLIDERPP = await GETSLIDER.json();

    console.log(GETSLIDERPP);
  };

  componentWillUnmount() {
    clearInterval(this.clockCall);
  }

  render() {
    const htmlContent = this.state.article.article;
    return (
      <ScrollView>
        <ModernHeader
            text={null}
            rightIconType="SimpleLineIcons"
            backgroundColor="#fdfdfd"
            rightIconName="menu"
            leftIconComponent={<HeaderLogo />}
            rightIconColor={COLOR1}
            rightIconOnPress={() => this.props.navigation.navigate('Menulist')}
        />
        <View style={{
          padding:20
        }}>
          <Text
            style={{
              textAlign: 'center',
              fontWeight: '700',
              fontSize: 18,
              textTransform: 'uppercase',
            }}>
            {this.state.article.title}
          </Text>

          <Text
            style={{
              textAlign: 'center',
              fontWeight: '700',
              fontSize: 10,
              color: '#bdbdbd',
              textTransform: 'uppercase',
            }}>
            {this.state.article.created_at}
          </Text>

          <View style={styles.lottieview}>
            {this.state.timer > 0 && (
              <LottieView
                style={styles.lottiefile}
                ref={animation => {
                  this.animation = animation;
                }}
                source={require('../../../../../Asset/lottie/6595-countdown-1')}
              />
            )}
          </View>

          <AdMobBanner
            adSize="banner"
            adUnitID="ca-app-pub-3269108089642244/9495046910"
            onAdFailedToLoad={error => console.error(error)}
           // testDevices={[AdMobBanner.simulatorId]}
          />

          <HTML html={htmlContent} imagesMaxWidth={Dimensions.get('window').width} />
        </View>

        <View
          style={{
            width: WIDTH,
          }}>
          {this.state.timer <= 0 && (
            <Button
              onPress={this.onTsdkcom}
              style={{
                width: 300,
                margin: 30,
                textAlign: 'center',
                alignContent: 'center',
                justifyContent: 'center',
                borderColor: COLOR4,
              }}
              status="primary">
              Complete
            </Button>
          )}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  lottiefile: {
    width: 100,
    height: 100,
    alignContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
  },
  lottieview: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 44,
  },
});

export default TaskDetails;
