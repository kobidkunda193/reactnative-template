import React, {Component} from 'react';
import {ListItem} from 'react-native-elements';
import {FlatList, View} from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {COLOR0, COLOR1, COLOR4} from '../../../../../Style/theme';
import {Button} from '@ui-kitten/components';
import AsyncStorage from '@react-native-community/async-storage';
import {inject, observer} from 'mobx-react';

let datalist = [
  {
    id: 1,
    title: 'Profile',
    icon: 'user',
    url: 'Profile',
  },
 /* {
    id: 2,
    title: 'Pending Transaction',
    icon: 'minus',
    url: 'PendingTxt',
  },*/
  {
    id: 3,
    title: 'Transaction',
    icon: 'check',
    url: 'CompleteTxt',
  },
   /*{
    id: 4,
    title: 'Share Profile',
    icon: 'share-alt',
    url: 'Profile',
  },*/
  {
    id: 7,
    title: 'Tree View',
    icon: 'list',
    url: 'TreeView',
  },
  {
    id: 5,
    title: 'Task',
    icon: 'login',
    url: 'DailyTask',
  }
];

@inject('Auth')
@observer
export default class Menulist extends Component {
  renderItem = ({item}) => (
    <ListItem
      title={item.title}
      leftIcon={<Icon name={`${item.icon}`} color={COLOR1} />}
      onPress={() => this.props.navigation.navigate(`${item.url}`)}
      bottomDivider
      chevron
    />
  );

  Logoutuser = async () => {
    await AsyncStorage.removeItem('@token_type');
    await AsyncStorage.removeItem('@access_token');
    await AsyncStorage.removeItem('@refresh_token');
    this.props.Auth.auth = false;
  }

  render() {
    return (
      <View>
        <FlatList
          keyExtractor={this.keyExtractor}
          data={datalist}
          renderItem={this.renderItem}
        />
        <Button
           onPress={this.Logoutuser}
          style={{
            width: 300,
            margin:30,
            textAlign:'center',
            alignContent:'center',
            justifyContent:'center',
            borderColor: COLOR4
          }}
          status="primary">
          Logout
        </Button>
      </View>
    );
  }
}
