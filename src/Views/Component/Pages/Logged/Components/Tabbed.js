import * as React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {COLOR1, COLOR4} from '../../../../../Style/theme';
import PendingTxt from './PendingTxt';
import DailyTask from './DailtTask';
const FirstRoute = () => (
  <DailyTask/>
);

const SecondRoute = () => (
    <PendingTxt/>
);

const initialLayout = {width: Dimensions.get('window').width};

const renderTabBar = props => (
  <TabBar
    {...props}
    bounces={true}
    tabStyle={styles.tabstyles}
    indicatorStyle={{backgroundColor: 'white'}}
    style={styles.tabbedcontainer}
    renderIcon={({route, focused, color}) => (
      <Icon name={focused ? 'control-play' : 'control-end'} color={color} />
    )}
  />
);

export default function Tabbed() {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'Daily Task'},
    {key: 'second', title: 'Pending'},
  ]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  return (
    <TabView
      renderTabBar={renderTabBar}
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
    />
  );
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  tabbedcontainer: {
    backgroundColor: COLOR4,
    borderRadius:5,
  },
  tabstyles:{
    borderRadius:5,
    padding:5
  }
});
