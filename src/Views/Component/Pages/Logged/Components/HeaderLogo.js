import React, {Component} from 'react';
import {Dimensions, Image, View} from 'react-native';
let HEIGHT = Dimensions.get('screen').height;
let WIDTH = Dimensions.get('screen').width;
export default class HeaderLogo extends Component {
  render() {
    return (
      <View>
        <Image
          style={{
            width: 100,
            height: 25,
          }}
          source={require('../../../../../Asset/Image/logo.png')}
        />
      </View>
    );
  }
}
