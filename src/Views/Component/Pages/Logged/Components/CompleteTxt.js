import React, {Component} from 'react';
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {
  ALLARTICLE,
  DATED_TXT,
  GETTASK,
  SLIDER,
  TXTPENDING,
} from '../../../../../Static/URL';
import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {
  COLOR0,
  COLOR1,
  COLOR4,
  COLOR5,
  COLORTEXTSM,
  SubContainer,
} from '../../../../../Style/theme';

let HEIGHT = Dimensions.get('screen').height;
let WIDTH = Dimensions.get('screen').width;
import {useNavigation} from '@react-navigation/native';
import HeaderLogo from './HeaderLogo';
import {ModernHeader} from '@freakycoder/react-native-header-view';
import {API_TOKEN} from '../../../../../Static/credintials';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import {Button} from '@ui-kitten/components';
class CompleteTxt extends Component {
  constructor(props) {
    super(props);

    this.state = {
      transactions: [],
      from: null,
      to: null,
      loading: false,
    };
  }

  async componentDidMount() {
    this.setState({
      loading: true,
    });
    const value = await AsyncStorage.getItem('@access_token');

    let GETSLIDER = await fetch(DATED_TXT, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
      body: JSON.stringify({
        start_date: moment()
          .utcOffset('+05:30')
          .format('DD-MM-YYYY'),
        end_date: moment()
          .utcOffset('+05:30')
          .format('DD-MM-YYYY'),
      }),
    });

    let GETSLIDERPP = await GETSLIDER.json();
    console.log(GETSLIDERPP);
    this.setState({
      transactions: GETSLIDERPP.transactions,
      loading: false,
    });
  }

  Ontxt = async () => {
    if (this.state.from === null || this.state.to === null) {
      alert('Please input Date ');
    }
    {
      this.setState({
        loading: true,
      });
      const value = await AsyncStorage.getItem('@access_token');

      let GETSLIDER = await fetch(DATED_TXT, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + value,
        },
        body: JSON.stringify({
          start_date: this.state.from,
          end_date: this.state.to,
        }),
      });

      let GETSLIDERPP = await GETSLIDER.json();
      console.log(GETSLIDERPP);
      this.setState({
        transactions: GETSLIDERPP.transactions,
        loading: false,
      });
    }
  };

  renderItem = ({item, navigation}) => (
    <ListItem
      style={{flex: 1}}
      title={'Task ' + item.type + '  ₹ ' + item.amount}
      titleStyle={{
        textTransform: 'uppercase',
      }}
      subtitle={item.description + ' | ' + item.created_at}
      bottomDivider
      chevron
    />
  );

  render() {
    return (
      <View
        style={{
          flex: 1,
          width: '100%',
        }}>
        <ModernHeader
          text={null}
          rightIconType="SimpleLineIcons"
          backgroundColor="#fdfdfd"
          rightIconName="menu"
          leftIconComponent={<HeaderLogo />}
          rightIconColor={COLOR1}
          rightIconOnPress={() => this.props.navigation.navigate('Menulist')}
        />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
          }}>
          <Text>From Date</Text>
          <DatePicker
            style={{width: 200}}
            date={this.state.from}
            mode="date"
            placeholder="select date"
            format="DD-MM-YYYY"
            maxDate={moment()
              .utcOffset('+05:30')
              .format('DD-MM-YYYY')}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0,
              },
              dateInput: {
                marginLeft: 36,
              },
              // ... You can check the source to find the other keys.
            }}
            onDateChange={date => {
              this.setState({from: date});
            }}
          />
        </View>

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
          }}>
          <Text>To Date</Text>
          <DatePicker
            style={{
              width: 200,
              justifyContent: 'center',
              alignItems: 'center',
              textAlign: 'center',
            }}
            date={this.state.to}
            mode="date"
            placeholder="select date"
            format="DD-MM-YYYY"
            maxDate={moment()
              .utcOffset('+05:30')
              .format('DD-MM-YYYY')}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0,
              },
              dateInput: {
                marginLeft: 36,
              },
              // ... You can check the source to find the other keys.
            }}
            onDateChange={date => {
              this.setState({to: date});
            }}
          />
        </View>

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
          }}>
          <Button
            icon={
              this.state.loading
                ? () => <ActivityIndicator color="#ffffff" />
                : null
            }
            onPress={this.Ontxt}
            style={{
              width: 200,
              borderColor: COLOR4,
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
              textAlign: 'center',
            }}
            status="primary">
            Search
          </Button>
        </View>
        <View
          style={{
            width: WIDTH,
          }}>
          <FlatList
            contentContainerStyle={{paddingBottom: 300}}
            style={{
              paddingBottom: 30,
            }}
            keyExtractor={this.keyExtractor}
            data={this.state.transactions}
            renderItem={this.renderItem}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
export default CompleteTxt;
