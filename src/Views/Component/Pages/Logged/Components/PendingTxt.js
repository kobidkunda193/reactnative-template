import React, {Component} from 'react';
import {Text, View, Dimensions, StyleSheet, FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {SLIDER, TXTPENDING} from '../../../../../Static/URL';
import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {COLOR1, COLORTEXTSM, SubContainer} from '../../../../../Style/theme';

let HEIGHT = Dimensions.get('screen').height;
let WIDTH = Dimensions.get('screen').width;

export default class PendingTxt extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  async componentDidMount() {
    const value = await AsyncStorage.getItem('@access_token');

    let GETSLIDER = await fetch(TXTPENDING, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + value,
      },
    });

    let GETSLIDERPP = await GETSLIDER.json();
    this.setState({
      data: GETSLIDERPP.data,
    });
  }

  renderItem = ({item}) => (
    <ListItem
      title={item.amount}
      description={item.description}
      // leftIcon={<Icon name={`${item.icon}`} color={COLOR1} />}
      // onPress={() => this.props.navigation.navigate(`${item.url}`)}
      bottomDivider
      chevron
    />
  );

  render() {
    if (this.state.data.length === 0) {
      return (
        <SubContainer>
          <COLORTEXTSM>No Transaction</COLORTEXTSM>
        </SubContainer>
      );
    } else {
      return (
        <View>
          <FlatList
            keyExtractor={this.keyExtractor}
            data={this.state.data}
            renderItem={this.renderItem}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({});
