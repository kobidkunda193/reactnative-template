import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {
  Container,
  COLORTEXTSM,
  TEXTLG,
  SubContainer,
} from '../../../Style/theme';
import LottieView from 'lottie-react-native';
import * as Animatable from 'react-native-animatable';

import {inject, observer} from 'mobx-react';
@inject("Auth")
@observer

export default class Loading extends Component {

    static   navigationOptions: {
        headerShown: false,
    }

    constructor(props) {
    super(props);

    this.state = {
      data: '',
    };
  }

  componentDidMount() {


    this.animation.play();
    // Or set a specific startFrame and endFrame with:
    this.animation.play(0, 333);
  }


  OnTaskComplete = async  () => {

  }

  render() {
    return (
      <Container>
        <SubContainer
          style={{
            flex: 3,
          }}>
          <Animatable.Image
            animation="pulse"
            easing="ease-in-out-cubic"
            iterationCount="infinite"
            style={{
              width: 250,
              height: 70,
            }}
            source={require('../../../Asset/Image/logowhite.png')}
          />
        </SubContainer>

        <SubContainer>
          <LottieView
            style={styles.lottiefile}
            ref={animation => {
              this.animation = animation;
            }}
            source={require('../../../Asset/lottie/lf30_editor_l54CXx.json')}
          />
        </SubContainer>
        <SubContainer>
          <TEXTLG style={{color: '#ffffff'}}>Loading</TEXTLG>
        </SubContainer>
        <SubContainer>
          <COLORTEXTSM style={{color: '#ffffff'}}>

          </COLORTEXTSM>
        </SubContainer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  lottiefile: {
    width: 100,
    height: 100,
  },
});
