import React from 'react';
import {ButtonPrimaryText, ButtonPrimary} from '../Style/theme';

const PrimaryButtonBG = props => (
  <ButtonPrimary backgroundColor={props.backgroundColor}>
    <ButtonPrimaryText textColor={props.textColor}>
      {props.text}
    </ButtonPrimaryText>
  </ButtonPrimary>
);

export default PrimaryButtonBG;
