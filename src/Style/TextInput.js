import React from 'react';
import {compose} from 'recompose';
import {
  handleTextInput,
  withNextInputAutoFocusInput,
} from 'react-native-formik';
import {Input} from '@ui-kitten/components';

class TextInput extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  focus = () => {
    this.refs.input.focus();
  };

  render() {
    return <Input ref="input" {...this.props} />;
  }
}




