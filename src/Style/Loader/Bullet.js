import React from 'react';
import ContentLoader, {Rect, Circle} from 'react-content-loader/native';

const Bullet = () => (
  <ContentLoader
    speed={3}
    width={310}
    height={250}
    viewBox="0 0 310 250"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb">
    <Circle cx="10" cy="20" r="8" />
    <Rect x="25" y="15" rx="5" ry="5" width="310" height="10" />
    <Circle cx="10" cy="50" r="8" />
    <Rect x="25" y="45" rx="5" ry="5" width="310" height="10" />
    <Circle cx="10" cy="80" r="8" />
    <Rect x="25" y="75" rx="5" ry="5" width="310" height="10" />
    <Circle cx="10" cy="110" r="8" />
    <Rect x="25" y="105" rx="5" ry="5" width="310" height="10" />
    <Circle cx="10" cy="140" r="8" />
    <Rect x="25" y="135" rx="5" ry="5" width="310" height="10" />
    <Circle cx="10" cy="170" r="8" />
    <Rect x="25" y="165" rx="5" ry="5" width="310" height="10" />
    <Circle cx="10" cy="200" r="8" />
    <Rect x="25" y="195" rx="5" ry="5" width="310" height="10" />
    <Circle cx="10" cy="230" r="8" />
    <Rect x="25" y="225" rx="5" ry="5" width="310" height="10" />

  </ContentLoader>
);

export default Bullet;
