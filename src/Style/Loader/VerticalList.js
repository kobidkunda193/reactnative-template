import React from 'react';
import ContentLoader, {Circle, Rect} from 'react-content-loader/native';

const VerticalList = () => (
  <ContentLoader
    speed={1}
    width={550}
    height={50}
    viewBox="0 0 600 100"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb">
    <Circle cx="70" cy="50" r="30" />
    <Rect x="0" y="90" rx="0" ry="0" width="140" height="25" />
    <Circle cx="230" cy="50" r="30" />
    <Rect x="160" y="90" rx="0" ry="0" width="140" height="25" />
    <Circle cx="390" cy="50" r="30" />
    <Rect x="320" y="90" rx="0" ry="0" width="140" height="25" />
    <Circle cx="550" cy="50" r="30" />
    <Rect x="400" y="90" rx="0" ry="0" width="140" height="25" />
  </ContentLoader>
);

export default VerticalList;
