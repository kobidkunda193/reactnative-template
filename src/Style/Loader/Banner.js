import React from 'react';
import ContentLoader, {Rect, Circle} from 'react-content-loader/native';

const Banner = () => (
  <ContentLoader
    speed={5}
    width={350}
    height={200}
    viewBox="0 0 350 200"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb">
    <Rect x="0" y="0" rx="2" ry="2" width="350" height="200" />
  </ContentLoader>
);

export default Banner;
