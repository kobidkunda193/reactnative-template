import React from 'react';
import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
export const HEIGHT = Dimensions.get('screen').height;
export const WIDTH = Dimensions.get('screen').width;

export const COLOR0 = '#282828';
export const COLOR1 = '#5033f3';
export const COLOR2 = '#ffffff';
export const COLOR3 = '#F6C851';
export const COLOR4 = '#EF823E';
export const COLOR5 = '#5ABC93';

export const TEXTLG = styled.Text`
  font-size: 34px
  font-weight: 700
  color: ${COLOR0}
`;
export const COLORTEXTSM = styled.Text`
  font-size: 12px
  font-weight: 300
  color: ${COLOR0}
`;

export const ButtonPrimary = styled.TouchableOpacity`
  background-color: ${COLOR4};
  width: 300px;
  height: 40px;
  border-radius: 3px;
   justify-content: center
  align-items: center
  elevation: 20
`;
export const ButtonPrimaryText = styled.Text`
  color: ${COLOR2};
`;

export const Container = styled.View`
  flex: 1;
  background-color: ${COLOR1};
  justify-content: center;
  align-items: center;
`;

export const ContainerN = styled.View`
  flex: 1;
  background-color: ${COLOR2};
  justify-content: center;
  align-items: center;
`;

export const SubContainer = styled.View`
  padding:10px
    justify-content: space-between
  justify-content: center
  align-items: center
`;

export const Title = styled.Text`
  font-size: 20px;
  font-weight: 500;
  color: palevioletred;
`;
