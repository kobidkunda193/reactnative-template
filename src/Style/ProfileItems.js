import React from 'react';
import {ButtonPrimaryText, ButtonPrimary} from '../Style/theme';
import {StyleSheet, Text, View} from 'react-native';
const ProfileItems = props => (
  <View style={styles.container}>
    <Text style={styles.text}>
      {' '}
      {props.heading} : {props.text}
    </Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    margin: 12,
    padding: 12,
    width: '100%',
    borderColor: '#5e40ff',
    borderWidth: 2,
    borderRadius: 22,
  },
  text: {
    fontSize: 16,
    fontWeight: '700',
    textTransform: 'uppercase',
    textAlign: 'left',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    color: '#ffffff',
  },
});
export default ProfileItems;
